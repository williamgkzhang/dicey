import RollLexer from "../src/parser/RollLexer"
import RollParser from "../src/parser/RollParser"

const testConfigurations = [
  {
    input: `5d4`,
    expectedValue: { numRolls: 5, sides: 4, bonus: 0 },
  },
  {
    input: `1d8+3`,
    expectedValue: { numRolls: 1, sides: 8, bonus: 3 },
  },
  {
    input: `20 d 20 - 54`,
    expectedValue: { numRolls: 20, sides: 20, bonus: -54 },
  },
]

const invalidInputs = [
  { input: `1.5d20` },
  { input: `1d6x4` },
  { input: `jimmy` },
]

describe(`RollParser`, () => {
  let parser: RollParser

  const parse = (testConfig): object => {
    // Tokenize
    const lexerResult = RollLexer.tokenize(testConfig.input)
    // Parse
    parser.input = lexerResult.tokens
    const parserResult = parser.roll()
    return parserResult
  }

  const performSuccessTest = (testConfig): void => {
    it(`should parse ${testConfig.input}`, () => {
      const parserResult = parse(testConfig)
      // Ensure parse results
      expect(parserResult).toEqual(testConfig.expectedValue)
    })
  }

  const performErrorTest = (testConfig): void => {
    it(`should throw error when trying to parse ${testConfig.input}`, () => {
      parse(testConfig)
      expect(parser.errors.length).toBeGreaterThan(0)
    })
  }

  beforeAll(() => {
    parser = new RollParser()
  })

  testConfigurations.forEach(testConfig => performSuccessTest(testConfig))
  invalidInputs.forEach(invalidInput => performErrorTest(invalidInput))
})
