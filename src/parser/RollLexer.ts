import { createToken, Lexer } from "chevrotain"

export const whiteSpaceToken = createToken({
  name: `whiteSpace`,
  pattern: /\s+/,
  group: Lexer.SKIPPED,
})

export const integerToken = createToken({
  name: `integer`,
  pattern: /0|[1-9]\d*/,
})

export const diceSeparatorToken = createToken({
  name: `diceSeparator`,
  pattern: /d/,
})
export const bonusOperatorToken = createToken({
  name: `bonusOperator`,
  pattern: /[+-]/,
})

export const tokens = [
  whiteSpaceToken,
  integerToken,
  diceSeparatorToken,
  bonusOperatorToken,
]

const RollLexer = new Lexer(tokens)
export default RollLexer
