import RollLexer from "./parser/RollLexer"
import RollParser from "./parser/RollParser"

/**
 * A detailed result of a dice roll
 * @property {number[]} rollResults - results of rolls
 * @property {number} bonus - bonus applied
 * @property {number} total - total of roll results and bonus
 */
interface Result {
  rollResults: Array<number>
  bonus: number
  total: number
}

/**
 * A POJO representing a dice roll
 * @property {integer} numRolls
 * @property {integer} sides
 * @property {integer} bonus
 */
interface RollObject {
  numRolls: number
  sides: number
  bonus: number
}

const parser = new RollParser()

/**
 * Class representing a dice roll
 * @property {integer} numRolls - Number of times the dice should be rolled.  Should be greater than 0.
 * @property {integer} sides    - Number of sides of the dice. Should be greater than 0.
 * @property {integer} bonus    - Bonus that should applied to a dice roll.
 */
class Roll {
  private _numRolls: number
  private _sides: number
  private _bonus: number

  constructor(numRolls: number, sides: number, bonus = 0) {
    this._numRolls = numRolls
    this._sides = sides
    this._bonus = bonus
  }

  static parse(input: string): Roll {
    // Tokenize
    const lexerResult = RollLexer.tokenize(input)

    if (lexerResult.errors.length)
      throw new Error(`Invalid characters in ${input}`)

    // Parse
    parser.input = lexerResult.tokens
    const parserResult = parser.roll()

    if (parser.errors.length) throw new Error(`Could not parse ${input}`)

    return this.fromObject(parserResult)
  }

  static fromObject(roll: RollObject): Roll {
    return new Roll(roll.numRolls, roll.sides, roll.bonus)
  }

  /** Accessor for number of times the dice should be rolled */
  get numRolls(): number {
    return this._numRolls
  }

  /** Setter for number of times the dice should be rolled */
  set numRolls(numRolls: number) {
    this._numRolls = numRolls
  }

  /**
   * Validates whether the number of rolls is valid
   * @throws if `numRolls` is not an integer
   * @throws if `numRolls` is less than or equal to 0
   */
  validateNumRolls(): void {
    const isInteger = Number.isInteger(this._numRolls)
    if (!isInteger) throw new Error(`numRolls should be integer`)
    if (this._numRolls <= 0) throw new Error(`numRolls should be above 0`)
  }

  /** Accessor for number of sides on a dice */
  get sides(): number {
    return this._sides
  }

  /** Setter for number of sides on a dice */
  set sides(sides: number) {
    this._sides = sides
  }

  /**
   * Validates whether the number of sides is valid
   * @throws if `sides` is not an integer
   * @throws if `sides` is less than or equal to 0
   */
  validateSides(): void {
    const isInteger = Number.isInteger(this._sides)
    if (!isInteger) throw new Error(`sides should be integer`)
    if (this._sides <= 0) throw new Error(`sides should be above 0`)
  }

  /** Accessor for the bonus that should be added to a dice roll */
  get bonus(): number {
    return this._bonus
  }

  /** Setter for the bonus that should be added to a dice roll */
  set bonus(bonus: number) {
    this._bonus = bonus
  }

  /**
   * Validates whether the bonus is valid
   * @throws if bonus is not an integer
   */
  validateBonus(): void {
    const isInteger = Number.isInteger(this._bonus)
    if (!isInteger) throw new Error(`bonus should be integer`)
  }

  /** Validates roll and throws error if invalid */
  validate(): void {
    this.validateBonus()
    this.validateSides()
    this.validateNumRolls()
  }

  /** Returns whether or not this roll is valid */
  get isValid(): boolean {
    try {
      this.validate()
      return true
    } catch (e) {
      return false
    }
  }

  /** Object representation of a roll */
  toObject(): object {
    return {
      numRolls: this._numRolls,
      sides: this._sides,
      bonus: this._bonus,
    }
  }

  /** JSON serialization */
  toJSON(): object {
    return this.toObject()
  }

  /** Returns the roll in standard dice notation */
  toString(): string {
    const baseString = `${this._numRolls}d${this._sides}`
    if (this.bonus === 0) {
      return baseString
    } else {
      const operator = this.bonus > 0 ? `+` : `-`
      const absBonus = Math.abs(this._bonus)
      return baseString + operator + absBonus
    }

    return this._bonus === 0 ? baseString : `${baseString}+${this._bonus}`
  }

  /**
   * Roll the dice
   * @returns {number} result of dice roll
   * @throws error if roll is invalid
   */
  roll(): number {
    const { total } = this.rollVerbose()
    return total
  }

  /**
   * Roll the dice and return a verbose set of results
   * @returns {Result} representation of dice roll
   * @throws error if roll is invalid
   */
  rollVerbose(): Result {
    this.validate()
    const rollResults: Array<number> = []

    // Gather Roll Results
    for (let i = 0; i < this._numRolls; i = i + 1) {
      rollResults.push(
        // Roll the dice
        Math.ceil(Math.random() * this._sides),
      )
    }
    // Add roll results and add bonus
    const total =
      rollResults.reduce((total, result) => total + result, 0) + this._bonus
    return {
      rollResults,
      bonus: this._bonus,
      total,
    }
  }
}

export default Roll
