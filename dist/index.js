(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('chevrotain')) :
  typeof define === 'function' && define.amd ? define(['exports', 'chevrotain'], factory) :
  (global = global || self, factory(global.dicey = {}, global.chevrotain));
}(this, function (exports, chevrotain) { 'use strict';

  var whiteSpaceToken = chevrotain.createToken({
      name: "whiteSpace",
      pattern: /\s+/,
      group: chevrotain.Lexer.SKIPPED,
  });
  var integerToken = chevrotain.createToken({
      name: "integer",
      pattern: /0|[1-9]\d*/,
  });
  var diceSeparatorToken = chevrotain.createToken({
      name: "diceSeparator",
      pattern: /d/,
  });
  var bonusOperatorToken = chevrotain.createToken({
      name: "bonusOperator",
      pattern: /[+-]/,
  });
  var tokens = [
      whiteSpaceToken,
      integerToken,
      diceSeparatorToken,
      bonusOperatorToken,
  ];
  var RollLexer = new chevrotain.Lexer(tokens);

  /*! *****************************************************************************
  Copyright (c) Microsoft Corporation. All rights reserved.
  Licensed under the Apache License, Version 2.0 (the "License"); you may not use
  this file except in compliance with the License. You may obtain a copy of the
  License at http://www.apache.org/licenses/LICENSE-2.0

  THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
  WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
  MERCHANTABLITY OR NON-INFRINGEMENT.

  See the Apache Version 2.0 License for specific language governing permissions
  and limitations under the License.
  ***************************************************************************** */
  /* global Reflect, Promise */

  var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf ||
          ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
          function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
      return extendStatics(d, b);
  };

  function __extends(d, b) {
      extendStatics(d, b);
      function __() { this.constructor = d; }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  }

  var RollParser = /** @class */ (function (_super) {
      __extends(RollParser, _super);
      function RollParser() {
          var _this = _super.call(this, tokens) || this;
          _this.roll = _this.RULE("roll", function () {
              var diceRoll = _this.SUBRULE(_this.diceRoll);
              var bonus = _this.OPTION(function () {
                  return _this.SUBRULE(_this.bonus);
              }) || { bonus: 0 };
              return Object.assign({}, diceRoll, bonus);
          });
          _this.diceRoll = _this.RULE("diceRoll", function () {
              var numRollsString = _this.CONSUME(integerToken).image;
              _this.CONSUME(diceSeparatorToken);
              var sidesString = _this.CONSUME1(integerToken).image;
              var numRolls = parseInt(numRollsString);
              var sides = parseInt(sidesString);
              return { numRolls: numRolls, sides: sides };
          });
          _this.bonus = _this.RULE("bonus", function () {
              var operatorString = _this.CONSUME(bonusOperatorToken).image;
              var bonusAmountString = _this.CONSUME(integerToken).image;
              var bonusModifier = operatorString === "-" ? -1 : 1;
              var bonusAmount = parseInt(bonusAmountString);
              return { bonus: bonusModifier * bonusAmount };
          });
          _this.performSelfAnalysis();
          return _this;
      }
      return RollParser;
  }(chevrotain.EmbeddedActionsParser));

  var parser = new RollParser();
  /**
   * Class representing a dice roll
   * @property {integer} numRolls - Number of times the dice should be rolled.  Should be greater than 0.
   * @property {integer} sides    - Number of sides of the dice. Should be greater than 0.
   * @property {integer} bonus    - Bonus that should applied to a dice roll.
   */
  var Roll = /** @class */ (function () {
      function Roll(numRolls, sides, bonus) {
          if (bonus === void 0) { bonus = 0; }
          this._numRolls = numRolls;
          this._sides = sides;
          this._bonus = bonus;
      }
      Roll.parse = function (input) {
          // Tokenize
          var lexerResult = RollLexer.tokenize(input);
          if (lexerResult.errors.length)
              throw new Error("Invalid characters in " + input);
          // Parse
          parser.input = lexerResult.tokens;
          var parserResult = parser.roll();
          if (parser.errors.length)
              throw new Error("Could not parse " + input);
          return this.fromObject(parserResult);
      };
      Roll.fromObject = function (roll) {
          return new Roll(roll.numRolls, roll.sides, roll.bonus);
      };
      Object.defineProperty(Roll.prototype, "numRolls", {
          /** Accessor for number of times the dice should be rolled */
          get: function () {
              return this._numRolls;
          },
          /** Setter for number of times the dice should be rolled */
          set: function (numRolls) {
              this._numRolls = numRolls;
          },
          enumerable: true,
          configurable: true
      });
      /**
       * Validates whether the number of rolls is valid
       * @throws if `numRolls` is not an integer
       * @throws if `numRolls` is less than or equal to 0
       */
      Roll.prototype.validateNumRolls = function () {
          var isInteger = Number.isInteger(this._numRolls);
          if (!isInteger)
              throw new Error("numRolls should be integer");
          if (this._numRolls <= 0)
              throw new Error("numRolls should be above 0");
      };
      Object.defineProperty(Roll.prototype, "sides", {
          /** Accessor for number of sides on a dice */
          get: function () {
              return this._sides;
          },
          /** Setter for number of sides on a dice */
          set: function (sides) {
              this._sides = sides;
          },
          enumerable: true,
          configurable: true
      });
      /**
       * Validates whether the number of sides is valid
       * @throws if `sides` is not an integer
       * @throws if `sides` is less than or equal to 0
       */
      Roll.prototype.validateSides = function () {
          var isInteger = Number.isInteger(this._sides);
          if (!isInteger)
              throw new Error("sides should be integer");
          if (this._sides <= 0)
              throw new Error("sides should be above 0");
      };
      Object.defineProperty(Roll.prototype, "bonus", {
          /** Accessor for the bonus that should be added to a dice roll */
          get: function () {
              return this._bonus;
          },
          /** Setter for the bonus that should be added to a dice roll */
          set: function (bonus) {
              this._bonus = bonus;
          },
          enumerable: true,
          configurable: true
      });
      /**
       * Validates whether the bonus is valid
       * @throws if bonus is not an integer
       */
      Roll.prototype.validateBonus = function () {
          var isInteger = Number.isInteger(this._bonus);
          if (!isInteger)
              throw new Error("bonus should be integer");
      };
      /** Validates roll and throws error if invalid */
      Roll.prototype.validate = function () {
          this.validateBonus();
          this.validateSides();
          this.validateNumRolls();
      };
      Object.defineProperty(Roll.prototype, "isValid", {
          /** Returns whether or not this roll is valid */
          get: function () {
              try {
                  this.validate();
                  return true;
              }
              catch (e) {
                  return false;
              }
          },
          enumerable: true,
          configurable: true
      });
      /** Object representation of a roll */
      Roll.prototype.toObject = function () {
          return {
              numRolls: this._numRolls,
              sides: this._sides,
              bonus: this._bonus,
          };
      };
      /** JSON serialization */
      Roll.prototype.toJSON = function () {
          return this.toObject();
      };
      /**
       * Roll the dice
       * @returns {number} result of dice roll
       * @throws error if roll is invalid
       */
      Roll.prototype.roll = function () {
          var total = this.rollVerbose().total;
          return total;
      };
      /**
       * Roll the dice and return a verbose set of results
       * @returns {Result} representation of dice roll
       * @throws error if roll is invalid
       */
      Roll.prototype.rollVerbose = function () {
          this.validate();
          var rollResults = [];
          // Gather Roll Results
          for (var i = 0; i < this._numRolls; i = i + 1) {
              rollResults.push(
              // Roll the dice
              Math.ceil(Math.random() * this._sides));
          }
          // Add roll results and add bonus
          var total = rollResults.reduce(function (total, result) { return total + result; }, 0) + this._bonus;
          return {
              rollResults: rollResults,
              bonus: this._bonus,
              total: total,
          };
      };
      return Roll;
  }());

  exports.Roll = Roll;

  Object.defineProperty(exports, '__esModule', { value: true });

}));
