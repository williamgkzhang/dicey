import { EmbeddedActionsParser } from "chevrotain";
declare class RollParser extends EmbeddedActionsParser {
    constructor();
    roll: (idxInCallingRule?: number, ...args: any[]) => {
        numRolls: number;
        sides: number;
    } & {
        bonus: number;
    };
    diceRoll: (idxInCallingRule?: number, ...args: any[]) => {
        numRolls: number;
        sides: number;
    };
    bonus: (idxInCallingRule?: number, ...args: any[]) => {
        bonus: number;
    };
}
export default RollParser;
